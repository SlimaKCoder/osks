from django.shortcuts import render,redirect
from django.db.models import Q

from datetime import datetime
from apps.panel.forms import SearchForm
from apps.students.models import StudentGroup
from ...models import Lecture
from ...forms import LectureForm


def lectures_search(request):
    form = SearchForm(request.POST or None)

    if form.is_valid():
        search = form.cleaned_data['search']

        try:
            search = datetime.strptime(search, '%d.%m.%Y')

            query = (Q(date=search))
        except ValueError:
            search = search.split('_')

            if len(search) == 1:
                query = Q(group__year=search[0])
            else:
                try:
                    if 1 <= int(search[1]) <= 12:
                        choice = search[1]
                    else:
                        raise ValueError
                except ValueError:
                    choice = StudentGroup.month_get_id(search[1])

                if (choice):
                    query = Q(group__year=search[0])
                    query.add(Q(group__month=choice), Q.AND)

                    if len(search) > 2:
                        query.add(Q(group__number=search[2]), Q.AND)
                else:
                    query = False
        if query:
            lectures = Lecture.objects.filter(query).all()\
                .select_related('topic', 'group', 'instructor')
        else:
            lectures = None
    else:
        lectures = None

    return render(request, 'panel/lectures/search.html', {'form': form, 'lectures': lectures})


def lectures_add(request):
    form = LectureForm(request.POST or None)

    form.fields['group'].queryset = StudentGroup.objects.all()

    if form.is_valid():
        form.save()
        return redirect("panel.lectures.list")

    return render(request, 'panel/lectures/add.html', {'form': form})


def lectures_edit(request, uuid):
    lecture = Lecture.objects.get(pk=uuid)
    form = LectureForm(request.POST or None, instance=lecture)

    form.fields['group'].queryset = StudentGroup.objects.filter(pk=lecture.group_id)
    form.fields['group'].disabled = True

    if form.is_valid():
        form.save()
        return redirect("panel.lectures.info", uuid=uuid)

    return render(request, 'panel/lectures/edit.html', {'form': form, 'id': id})


def lectures_delete(request, uuid):
    lecture = Lecture.objects.get(pk=uuid)
    lecture.delete()

    return redirect('panel.lectures.list')
