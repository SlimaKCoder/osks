from django.shortcuts import render, redirect
from apps.students.models import StudentGroup
from ...forms import LectureForm


def students_groups_lectures_add(request, uuid):
    form = LectureForm(request.POST or None, initial={'group': id})

    query_group = StudentGroup.objects.filter(pk=uuid)
    form.fields['group'].queryset = query_group
    form.fields['group'].disabled = True

    if form.is_valid():
        form.save()
        return redirect("panel.students.groups.info", uuid=uuid)

    return render(request, 'panel/students/groups/lectures/add.html', {'form': form, 'group': query_group.first()})
