from .lectures import *
from .lectures_topics import *
from .students_lectures import *
from .students_groups_lectures import *
