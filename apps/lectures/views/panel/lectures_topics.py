from django.shortcuts import render, redirect
from ...models import LectureTopic
from ...forms import LectureTopicForm


def lectures_topics_list(request):
    topics = LectureTopic.objects.order_by('topic').all()

    return render(request, 'panel/lectures/topics/list.html', {'topics': topics})


def lectures_topics_add(request):
    form = LectureTopicForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect("panel.lectures.topics.list")

    return render(request, 'panel/lectures/topics/add.html', {'form': form})


def lectures_topics_edit(request, uuid):
    lecturetopic = LectureTopic.objects.get(pk=uuid)
    form = LectureTopicForm(request.POST or None, instance=lecturetopic)

    if form.is_valid():
        form.save()
        return redirect("panel.lectures.topics.list")

    return render(request, 'panel/lectures/topics/edit.html', {'form': form, 'id': id})


def lectures_topics_delete(request, uuid):
    lecturetopic = LectureTopic.objects.get(pk=uuid)
    lecturetopic.delete()

    return redirect('panel.lectures.topics.list')
