from django.shortcuts import render,redirect
from apps.students.models import Student
from ...models import StudentLecture
from ...forms import StudentLectureForm


def students_lectures_list(request):
    # lectures = Lecture.objects.order_by('surname').all()

    return render(request, 'students/lectures/list.html')


def students_lectures_add(request):
    form = StudentLectureForm(request.POST or None)

    form.fields['student'].queryset = Student.objects.order_by('-id')[:30]

    if form.is_valid():
        form.save()
        return redirect("panel.students.lectures.list")

    return render(request, 'students/lectures/add.html', {'form': form})


def students_lectures_edit(request, uuid):
    lecture = StudentLecture.objects.get(pk=uuid)
    form = StudentLectureForm(request.POST or None, instance=lecture)

    form.fields['student'].queryset = Student.objects.filter(pk=lecture.student_id)
    form.fields['student'].disabled = True

    if form.is_valid():
        form.save()
        return redirect("panel.students.lectures.list", uuid=uuid)

    return render(request, 'students/lectures/edit.html', {'form': form, 'id': id})


def students_lectures_delete(request, uuid):
    lecture = StudentLecture.objects.get(pk=uuid)
    lecture.delete()

    return redirect('panel.students.lectures.list')
