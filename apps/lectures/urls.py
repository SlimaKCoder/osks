from django.urls import path, include

from main.urls import panel_url_prefix

from .views import panel as panel_views

urlpatterns = [
    path(panel_url_prefix, include([
        path('wyklad/szukaj', panel_views.lectures_search, name='panel.lectures.search'),
        path('wyklad/dodaj', panel_views.lectures_add, name='panel.lectures.add'),
        path('wyklad/<str:uuid>/edytuj', panel_views.lectures_edit, name='panel.lectures.edit'),
        path('wyklad/<str:uuid>/usun', panel_views.lectures_delete, name='panel.lectures.delete'),

        path('wyklady/tematy', panel_views.lectures_topics_list, name='panel.lectures.topics.list'),
        path('wyklady/temat/dodaj', panel_views.lectures_topics_add, name='panel.lectures.topics.add'),
        path('wyklady/temat/<str:uuid>/edytuj', panel_views.lectures_topics_edit, name='panel.lectures.topics.edit'),
        path('wyklady/temat/<str:uuid>/usun', panel_views.lectures_topics_delete, name='panel.lectures.topics.delete'),

        path('kursanci/grupa/<str:uuid>/wyklad/dodaj',
             panel_views.students_groups_lectures_add,
             name='panel.students.groups.lectures.add'),

        path('wyklady/indywidualne',
             panel_views.students_lectures_list,
             name='panel.students.lectures.list'),
        path('wyklad/indywidualny/dodaj',
             panel_views.students_lectures_add,
             name='panel.students.lectures.add'),
        path('wyklad/indywidualny/<str:uuid>/edytuj',
             panel_views.students_lectures_edit,
             name='panel.students.lectures.edit'),
        path('wyklad/indywydyalny/<str:uuid>/usun',
             panel_views.students_lectures_delete,
             name='panel.students.lectures.delete'),
    ]))
]