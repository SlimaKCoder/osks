from django import forms
from main.widgets import TimeWidget, DateWidget
from .models import Lecture, StudentLecture, LectureTopic


class LectureTopicForm(forms.ModelForm):
    class Meta:
        model = LectureTopic
        fields = '__all__'


class LectureForm(forms.ModelForm):
    class Meta:
        model = Lecture
        fields = '__all__'
        widgets = {
            'date': DateWidget(),
            'start_time': TimeWidget(),
            'end_time': TimeWidget()
        }


class StudentLectureForm(forms.ModelForm):
    class Meta:
        model = StudentLecture
        fields = '__all__'
        widgets = {
            'date': DateWidget(),
            'start_time': TimeWidget(),
            'end_time': TimeWidget()
        }
