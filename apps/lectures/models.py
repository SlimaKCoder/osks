from django.db import models
from main.models import BaseModel
from apps.students.models import Student, StudentGroup
from apps.instructors.models import Instructor


class LectureTopic(BaseModel):
    topic = models.CharField(max_length=255)

    def __str__(self):
        return self.topic


class Lecture(BaseModel):
    instructor = models.ForeignKey(
        Instructor,
        models.SET_NULL,
        null=True,
        related_name='lectures'
    )
    group = models.ForeignKey(
        StudentGroup,
        models.CASCADE,
        related_name='lectures'
    )
    topic = models.ForeignKey(
        LectureTopic,
        models.SET_NULL,
        null=True
    )
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()

    class Meta:
        ordering = ['topic']


class StudentLecture(BaseModel):
    instructor = models.ForeignKey(
        Instructor,
        models.SET_NULL,
        null=True,
        related_name='student_lectures'
    )
    student = models.ForeignKey(
        Student,
        models.CASCADE,
        related_name='student_lectures'
    )
    topic = models.ForeignKey(
        LectureTopic,
        models.CASCADE
    )
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()

    class Meta:
        ordering = ['topic']
