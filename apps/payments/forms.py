from django import forms
from main.widgets import DateWidget
from .models import Payment, CoursePrice


class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = '__all__'
        widgets = {
            'date': DateWidget()
        }


class CoursePriceForm(forms.ModelForm):
    class Meta:
        model = CoursePrice
        fields = '__all__'
