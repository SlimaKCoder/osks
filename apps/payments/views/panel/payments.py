from django.shortcuts import render, redirect
from datetime import date, timedelta
from apps.students.models import Student
from ...models import Payment
from ...forms import PaymentForm


def payments_list(request):
    last_week = date.today() - timedelta(days=7)
    payments = Payment.objects.filter(date__gte=last_week).all().select_related('student', 'instructor')

    return render(request, 'panel/payments/list.html', {'payments': payments})


def payments_add(request):
    form = PaymentForm(request.POST or None)

    form.fields['student'].queryset = Student.objects.order_by('-id')[:30]

    if form.is_valid():
        form.save()
        return redirect("payment.list")

    return render(request, 'panel/payments/add.html', {'form': form})


def payments_edit(request, uuid):
    payment = Payment.objects.get(pk=uuid)
    form = PaymentForm(request.POST or None, instance=payment)

    form.fields['student'].queryset = Student.objects.filter(pk=payment.student_id)
    form.fields['student'].disabled = True

    if form.is_valid():
        form.save()
        return redirect("panel.payments.list")

    return render(request, 'panel/payments/edit.html', {'form': form, 'id': id})


def payments_delete(request, uuid):
    payment = Payment.objects.get(pk=uuid)
    payment.delete()

    return redirect('payment.list')

