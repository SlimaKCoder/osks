from django.shortcuts import render, redirect

from ...models import CoursePrice
from ...forms import CoursePriceForm


def prices_list(request):
    prices = CoursePrice.objects.order_by('price').all()

    return render(request, 'panel/prices/list.html', {'prices': prices})


def prices_add(request):
    form = CoursePriceForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect("panel.prices.list")

    return render(request, 'panel/prices/add.html', {'form': form})


def prices_edit(request, uuid):
    price = CoursePrice.objects.get(pk=uuid)
    form = CoursePriceForm(request.POST or None, instance=price)

    if form.is_valid():
        form.save()
        return redirect("panel.prices.list")

    return render(request, 'panel/prices/edit.html', {'form': form, 'id': id})


def prices_delete(request, uuid):
    price = CoursePrice.objects.get(pk=uuid)
    price.delete()

    return redirect('price.list')
