from django.shortcuts import render, redirect
from uuid import uuid4, UUID
from base64 import urlsafe_b64decode, urlsafe_b64encode
from apps.students.models import Student
from ...forms import PaymentForm


def students_payments_add(request, uuid):
    converted_uuid = UUID(bytes=urlsafe_b64decode(uuid + '=='))
    # TODO: Rewrite how model form sees uuids
    form = PaymentForm(request.POST or None, initial={'student': converted_uuid})

    form.fields['student'].queryset = Student.objects.filter(pk=uuid)
    #form.fields['student'].disabled = True

    if form.is_valid():
        form.save()
        return redirect("panel.students.info", uuid=uuid)

    return render(request, 'panel/students/payments/add.html', {'form': form, 'student_uuid': uuid})
