from django.urls import path, include

from main.urls import panel_url_prefix

from .views import panel as panel_views

urlpatterns = [
    path(panel_url_prefix, include([
        path('platnosc/szukaj', panel_views.payments_list, name='panel.payments.list'),
        path('platnosc/dodaj', panel_views.payments_add, name='panel.payments.add'),
        path('platnosc/<str:uuid>/edytuj', panel_views.payments_edit, name='panel.payments.edit'),
        path('platnosc/<str:uuid>/usun', panel_views.payments_delete, name='panel.payments.delete'),

        path('kursant/<str:uuid>/platnosc/dodaj/', panel_views.students_payments_add, name='panel.students.payments.add'),

        path('stawki', panel_views.prices_list, name='panel.prices.list'),
        path('stawka/dodaj', panel_views.prices_add, name='panel.prices.add'),
        path('stawka/<str:uuid>/edytuj', panel_views.prices_edit, name='panel.prices.edit'),
        path('stawka/<str:uuid>/usun', panel_views.prices_delete, name='panel.prices.delete'),
    ]))
]
