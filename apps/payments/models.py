from django.db import models
from main.models import BaseModel


class CoursePrice(BaseModel):
    price = models.IntegerField(unique=True)

    class Meta:
        ordering = ['price']

    def __str__(self):
        return str(self.price)


class Payment(BaseModel):
    from apps.students.models import Student
    from apps.instructors.models import Instructor

    instructor = models.ForeignKey(
        Instructor,
        models.SET_NULL,
        null=True
    )
    student = models.ForeignKey(
        Student,
        models.CASCADE,
        related_name='payments'
    )
    date = models.DateField()
    value = models.IntegerField()

    class Meta:
        ordering = ['-date']
