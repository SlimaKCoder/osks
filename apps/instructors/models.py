from django.db import models
from main.models import User


class Instructor(User):
    irn = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return '%s %s' % (self.name, self.surname)
