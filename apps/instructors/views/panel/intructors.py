from apps.panel.utils.views import *
from ...models import Instructor
from ...forms import InstructorForm


class InstructorAddView(BaseAddView):
    model_class = Instructor
    form_class = InstructorForm
    url_save = "panel.instructors.list"
    url_return = "panel.instructors.list"


class InstructorEditView(BaseEditView):
    model_class = Instructor
    form_class = InstructorForm
    url_save = "panel.instructors.list"
    url_return = "panel.instructors.list"
    url_delete = "panel.instructors.delete"


class InstructorDeleteView(BaseDeleteView):
    model_class = Instructor
    url_save = "panel.instructors.list"


def instructors_list(request):
    instructors = Instructor.objects.all()

    return render(request, 'panel/instructors/list.html', {'instructors': instructors})


def instructors_info(request, uuid):
    return render(request, 'panel/students/add.html')

