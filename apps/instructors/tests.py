from .models import Instructor


def logged_and_admin(request):
    if request.is_authenticated():
        try:
            return isinstance(request.instructor, Instructor)
        except:
            return False
    else:
        return False
