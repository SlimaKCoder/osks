from django import forms
from .models import Instructor


class InstructorForm(forms.ModelForm):
    class Meta:
        model = Instructor
        fields = [
            'name',
            'surname',
            'pesel',
            'phone',
            'irn',
        ]
        field_order = fields
