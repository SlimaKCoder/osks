from django.urls import path, include
from .views.panel import *

from main.urls import panel_url_prefix

urlpatterns = [
    path(panel_url_prefix, include([
        path('instruktorzy/', instructors_list, name='panel.instructors.list'),
        path('instruktor/<str:uuid>/', instructors_info, name='panel.instructors.info'),
        # path('instruktor/<str:uuid>/platnosci$', instructor_payments, name='panel.instructors.payments'),
        # path('instruktor/<str:uuid>/wyklady$', instructor_lecture, name='panel.instructors.lectures'),
        # path('instruktor/<str:uuid>/jazdy$', instructor_drives, name='panel.instructors.drives'),

        path('instruktor/dodaj', InstructorAddView.as_view(), name='panel.instructors.add'),
        path('instruktor/<str:uuid>/edytuj', InstructorEditView.as_view(), name='panel.instructors.edit'),
        path('instruktor/<str:uuid>/usun', InstructorDeleteView.as_view(), name='panel.instructors.delete'),
    ]))
]