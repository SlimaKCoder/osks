from django.urls import path, include
from main.urls import portal_url_prefix
from . import views

urlpatterns = [
    path(portal_url_prefix, include([
        path('', views.index, name='portal.index'),
    ]))
]