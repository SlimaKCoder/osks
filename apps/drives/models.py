from django.db import models
from apps.instructors.models import Instructor
from apps.students.models import Student


class Drive(models.Model):
    instructor = models.ForeignKey(
        Instructor,
        models.SET_NULL,
        null=True,
        related_name='drives'
    )
    student = models.ForeignKey(
        Student,
        models.CASCADE,
        related_name='drives'
    )
    date = models.DateField()
    start_time = models.TimeField()
    end_time = models.TimeField()

    class Meta:
        ordering = ['-date', '-start_time']
