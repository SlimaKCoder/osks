from django import forms
from main.widgets import DateWidget, TimeWidget
from .models import Drive


class DriveForm(forms.ModelForm):
    class Meta:
        model = Drive
        fields = '__all__'
        widgets = {
            'date': DateWidget(),
            'start_time': TimeWidget(),
            'end_time': TimeWidget()
        }
