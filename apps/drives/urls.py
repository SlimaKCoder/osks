from django.urls import path, include
from main.urls import panel_url_prefix
from apps.drives.views.panel import *

urlpatterns = [
    path(panel_url_prefix, include([
        path('jazda/szukaj', drives_search, name='panel.drives.search'),
        path('jazda/dodaj', DriveAddView.as_view(), name='panel.drives.add'),
        path('jazda/<str:uuid>/edytuj', DriveEditView.as_view(), name='panel.drives.edit'),
        path('jazda/<str:uuid>/usun', DriveDeleteView.as_view(), name='panel.drives.delete'),

        path('kursant/<str:uuid>/jazda/dodaj/', StudentDriveAddView.as_view(), name='panel.students.drives.add'),
    ]))
]
