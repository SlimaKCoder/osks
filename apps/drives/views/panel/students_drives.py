from apps.students.models import Student
from apps.panel.utils.views import *
from ...forms import DriveForm


class StudentDriveAddView(BaseAddView):
    model_class = Student
    form_class = DriveForm
    url_save = "panel.students.info"
    url_return = "panel.students.search"
    _template = 'panel/forms/form_add.html'

    def set_form(self, request, *args, **kwargs):
        self.form = self.form_class(request.POST or None, initial={'student': kwargs['uuid']})
        self.form.fields['student'].queryset = self.model_class.objects.filter(pk=kwargs['uuid'])
        self.form.fields['student'].disabled = True

    def get_render_params(self, *args, **kwargs):
        render_params = super().get_render_params()
        render_params['student_id'] = kwargs['uuid']
        return render_params

    def post(self, request, *args, **kwargs):
        self.url_save_kwargs = kwargs
        return super().post(request, **kwargs)


