from django.shortcuts import render, redirect
from django.db.models import Q
from datetime import datetime
from apps.students.models import Student
from apps.panel.forms import SearchForm
from apps.panel.utils.views import *
from ...models import Drive
from ...forms import DriveForm


class DriveAddView(BaseAddView):
    model_class = Drive
    form_class = DriveForm
    url_save = "panel.drives.list"
    url_return = "panel.drives.search"

    def set_form(self, request):
        super().set_form(request)
        self._form.fields['student'].queryset = Student.objects.all()[:20]


class DriveEditView(BaseEditView):
    model_class = Drive
    form_class = DriveForm
    url_save = "panel.drives.list"
    url_return = "panel.drives.search"
    url_delete = "panel.drives.delete"

    def set_form(self, request, uuid):
        super().set_form(request, uuid)
        self._form.fields['student'].queryset = Student.objects.filter(pk=self._instance.student_id)
        self._form.fields['student'].disabled = True


class DriveDeleteView(BaseDeleteView):
    model_class = Drive
    url_save = "panel.instructors.list"


def drives_search(request):
    form = SearchForm(request.POST or None)

    if form.is_valid():
        search = form.cleaned_data['search']

        query = Q(student__surname__startswith=search)
        query.add(Q(student__pesel__startswith=search), Q.OR)

        try:
            search = datetime.strptime(search, '%d.%m.%Y')

            query.add(Q(date=search), Q.OR)
        except:
            pass

        drives = Drive.objects.filter(query).all() \
            .select_related('student', 'instructor')
    else:
        drives = None

    return render(request, 'panel/drives/search.html', {'form': form, 'drives': drives})
