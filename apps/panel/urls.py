from django.urls import path, include
from main.urls import panel_url_prefix
from . import views

urlpatterns = [
    path(panel_url_prefix, include([
        path('', views.index, name='panel.index'),
    ]))
]
