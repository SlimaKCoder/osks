from django.views import View
from django.db.models import Sum, Q
from django.shortcuts import render, redirect
from django.urls import reverse


class BaseAddView(View):
    model_class = None
    form_class = None
    url_save = None
    url_save_kwargs = None
    url_return = None
    url_return_kwargs = None
    _template = 'panel/forms/form_add.html'
    _form = None

    def dispatch(self, request, *args, **kwargs):
        self.set_form(request, *args, **kwargs)
        return super().dispatch(request, *args, **kwargs)

    def set_form(self, request, *args, **kwargs):
        self._form = self.form_class(request.POST or None)

    def get_render_params(self, *args, **kwargs):
        return {
            'form': self._form,
            'urls': {
                "return": reverse(self.url_return, kwargs=self.url_return_kwargs)
            }
        }

    def post(self, request, *args, **kwargs):
        if self._form.is_valid():
            self._form.save()
            return redirect(
                reverse(self.url_save, kwargs=self.url_save_kwargs))

        return self.get(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return render(
            request,
            self._template,
            self.get_render_params(*args, **kwargs)
        )


class BaseEditView(View):
    model_class = None
    form_class = None
    url_save = None
    url_save_kwargs = None
    url_return = None
    url_return_kwargs = None
    _template = 'panel/forms/form_edit.html'
    _form = None
    _instance = None

    def dispatch(self, request, *args, **kwargs):
        self.set_form(request, *args, **kwargs)
        return super().dispatch(request, *args, **kwargs)

    def set_form(self, request, uuid, *args, **kwargs):
        self._instance = self.model_class.objects.get(pk=uuid)
        self._form = self.form_class(request.POST or None, instance=self._instance)

    def get_render_params(self, uuid, *args, **kwargs):
        return {
            'form': self._form,
            'uuid': uuid,
            'urls': {
                "return": reverse(self.url_return, kwargs=self.url_return_kwargs)
            }
        }

    def post(self, request, uuid, *args, **kwargs):
        if self._form.is_valid():
            self._form.save()
            return redirect(
                reverse(self.url_save, kwargs=self.url_save_kwargs))

        return self.get(request, uuid, *args, **kwargs)

    def get(self, request, uuid, *args, **kwargs):
        return render(
            request,
            self._template,
            self.get_render_params(uuid, *args, **kwargs)
        )


class BaseDeleteView(View):
    model_class = None
    url_save = None
    url_save_kwargs = None

    def get_instance(self, uuid, *args, **kwargs):
        return self.model_class.objects.get(pk=uuid)

    def get(self, request, uuid, *args, **kwargs):
        instance = self.get_instance(self, request, uuid, *args, **kwargs)
        instance.delete()

        return redirect(self.url_save, kwargs=self.url_save_kwargs)


class BaseInfoView(View):
    model_class = None
    template = None
    _render_params = None

    def get_render_params(self, uuid, *args, **kwargs):
        return {
            "instance": self.get_instance(uuid, *args, **kwargs)
        }

    def get_instance(self, uuid, *args, **kwargs):
        return self.model_class.objects.get(pk=uuid)

    def get(self, request, uuid, *args, **kwargs):
        return render(
            request,
            self.template,
            self.get_render_params(uuid, *args, **kwargs)
        )


class BaseSearchView(View):
    form_class = None
    model_class = None
    template = None
    _form = None

    def dispatch(self, request, *args, **kwargs):
        self.set_form(request, *args, **kwargs)
        return super().dispatch(request, *args, **kwargs)

    def set_form(self, request, *args, **kwargs):
        self._form = self.form_class(request.POST or None)

    def get_search(self, request, *args, **kwargs):
        return self._form.cleaned_data['search']

    def get_query(self, *args, **kwargs):
        return Q()

    def get_instances(self, *args, **kwargs):
        return self.model_class.objects.filter(
            self.get_query(self, *args, **kwargs)
        ).all()

    def get_render_params(self, instances, *args, **kwargs):
        return {
            'form': self._form,
            "instances": instances
        }

    def post(self, request, *args, **kwargs):
        if self._form.is_valid():
            instances = self.get_instances(*args, **kwargs)
        else:
            instances = None

        return render(
            request,
            self.template,
            self.get_render_params(instances, *args, **kwargs)
        )

    def get(self, request, *args, **kwargs):
        return render(
            request,
            self.template,
            self.get_render_params(None, *args, **kwargs)
        )







