from django.shortcuts import render, redirect
from django.views.decorators.cache import cache_page


@cache_page(60 * 15)
def index(request):
    menu_items = [
        {
            'name': 'Grupy',
            'route': 'panel.students.groups.list',
            'icon': 'fa-users'
        },
        {
            'name': 'Kursanci',
            'route': 'panel.students.search',
            'icon': 'fa-graduation-cap'
        },
        {
            'name': 'Instruktorzy',
            'route': 'panel.instructors.list',
            'icon': 'fa-wheelchair'
        },
        {
            'name': 'Wykłady',
            'route': 'panel.lectures.search',
            'icon': 'fa-calendar-check-o'
        },
        {
            'name': 'Tematy',
            'route': 'panel.lectures.topics.list',
            'icon': 'fa-list-ol'
        },
        {
            'name': 'Jazdy',
            'route': 'panel.drives.search',
            'icon': 'fa-taxi'
        },
        {
            'name': 'Płatności',
            'route': 'panel.payments.list',
            'icon': 'fa-book'
        },
        {
            'name': 'Stawki',
            'route': 'panel.prices.list',
            'icon': 'fa-money'
        }
    ]

    return render(request, 'panel/index.html', {'menu_items': menu_items})
