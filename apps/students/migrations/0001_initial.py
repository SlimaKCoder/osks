# Generated by Django 2.1.3 on 2018-11-29 15:59

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import main.managers


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('main', '0001_initial'),
        ('payments', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('user_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('pkk', models.CharField(max_length=255, unique=True)),
                ('address', models.CharField(max_length=255)),
            ],
            options={
                'abstract': False,
            },
            bases=('main.user',),
            managers=[
                ('objects', main.managers.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='StudentGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start_date', models.DateField(auto_now=True)),
            ],
        ),
        migrations.AddField(
            model_name='student',
            name='group',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='students', to='students.StudentGroup'),
        ),
        migrations.AddField(
            model_name='student',
            name='individual_price',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='payments.CoursePrice'),
        ),
    ]
