from django.shortcuts import render, redirect
from ...models import StudentGroup
from ...forms import StudentForm


def students_groups_students_add(request, uuid):
    form = StudentForm(request.POST or None, initial={'group': id})

    query_group = StudentGroup.objects.filter(pk=uuid)
    form.fields['group'].queryset = query_group
    form.fields['group'].disabled = True

    if form.is_valid():
        form.save()
        return redirect("panel.students.groups.info", uuid=uuid)

    return render(request, 'panel/students/groups/students/add.html', {'form': form, 'group': query_group.first()})
