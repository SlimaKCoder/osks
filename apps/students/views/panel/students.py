from apps.panel.forms import SearchForm
from apps.panel.utils.views import *
from ...models import Student
from ...forms import StudentForm

from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.hashers  import make_password


class StudentAddView(BaseAddView):
    model_class = Student
    form_class = StudentForm
    url_save = "panel.students.search"
    url_return = "panel.students.search"


class StudentEditView(BaseEditView):
    model_class = Student
    form_class = StudentForm
    url_save = "panel.students.search"
    url_return = "panel.students.info"
    url_delete = "panel.students.delete"

    def post(self, request, uuid, *args, **kwargs):
        if self._form.is_valid():
            self._form.save()
            update_session_auth_hash(request, self._instance)
            return redirect(self.url_save)
        return self.get(request, uuid)

    def get(self, request, uuid, *args, **kwargs):
        self.url_return_kwargs = {"uuid": uuid}
        return super().get(request, uuid)


class StudentDeleteView(BaseDeleteView):
    model_class = Student
    url_save = "panel.students.search"


class StudentSearchView(BaseSearchView):
    form_class = SearchForm
    model_class = Student
    template = "panel/students/search.html"

    def get_query(self, *args, **kwargs):
        search = self.get_search(*args, **kwargs)
        query = (Q(name__startswith=search))
        query.add(Q(surname__startswith=search), Q.OR)
        query.add(Q(pesel__startswith=search), Q.OR)
        return query

    def get_instances(self, *args, **kwargs):
        return super().get_instances(*args, **kwargs).select_related('group')
        # .annotate(paid=Sum('payment__value'))


class StudentInfoView(BaseInfoView):
    model_class = Student
    template = "panel/students/info.html"

    def get_instance(self, uuid, *args, **kwargs):
        return self.model_class.objects \
            .select_related('group', 'individual_price') \
            .prefetch_related(
                'payments__instructor',
                'drives__instructor',
                'group__lectures__instructor',
                'group__lectures__topic',
                'student_lectures__instructor',
                'student_lectures__topic'
            ) \
            .annotate(paid=Sum('payments__value')) \
            .get(pk=uuid)

