from django.shortcuts import render,redirect
from ...models import StudentGroup
from ...forms import StudentGroupForm, StudentForm


def students_groups_list(request):
    groups = StudentGroup.objects.all()

    return render(request, 'panel/students/groups/list.html', {'groups': groups})


def students_groups_info(request, uuid):
    group = StudentGroup.objects.get(pk=uuid)

    return render(request, 'panel/studenst/groups/info.html', {'group': group})


def students_groups_add(request):
    form = StudentGroupForm(request.POST or None)

    if form.is_valid():
        form.save()
        return redirect("panel.students.groups.list")

    return render(request, 'panel/students/groups/add.html', {'form': form})


def students_groups_edit(request, uuid):
    group = StudentGroup.objects.get(pk=uuid)
    form = StudentGroupForm(request.POST or None, instance=group)

    if form.is_valid():
        form.save()
        return redirect("panel.students.groups.list")

    return render(request, 'panel/students/groups/edit.html', {'form': form, 'id': id})


def students_groups_delete(request, uuid):
    group = StudentGroup.objects.get(pk=uuid)
    group.delete()

    return redirect('panel.students.groups')
