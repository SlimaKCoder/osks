from .models import Student


def logged_and_student(request):
    if request.is_authenticated():
        try:
            return isinstance(request.student, Student)
        except:
            return False
    else:
        return False
