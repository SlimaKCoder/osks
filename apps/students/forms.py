from django import forms
from main.widgets import DateWidget
from .models import Student, StudentGroup


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = [
            'name',
            'surname',
            'pesel',
            'pkk',
            'address',
            'phone',
            'group',
            'individual_price',
        ]
        field_order = fields
        labels = {
            'name': 'Imię',
            'surname': 'Nazwisko',
            'address': 'Adres',
            'phone': 'Telefon',
            'group': 'Grupa',
            'individual_price': 'Spersonalizowana cena kursu',
        }


class StudentGroupForm(forms.ModelForm):
    class Meta:
        model = StudentGroup
        fields = '__all__'
        widgets = {
            'start_date': DateWidget(),
        }
