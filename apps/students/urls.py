from django.urls import path, include
from main.urls import panel_url_prefix
from .views import panel as panel_views
from .views.panel import *

urlpatterns = [
    path(panel_url_prefix, include([
        path('kursant/szukaj', StudentSearchView.as_view(), name='panel.students.search'),
        path('kursant/dodaj', StudentAddView.as_view(), name='panel.students.add'),
        path('kursant/<str:uuid>', StudentInfoView.as_view(), name='panel.students.info'),
        path('kursant/<str:uuid>/edytuj', StudentEditView.as_view(), name='panel.students.edit'),
        path('kursant/<str:uuid>/usun', StudentDeleteView.as_view(), name='panel.students.delete'),

        path('kursanci/grupy', panel_views.students_groups_list, name='panel.students.groups.list'),
        path('kursanci/grupa/dodaj', panel_views.students_groups_add, name='panel.students.groups.add'),
        path('kursanci/grupa/<str:uuid>', panel_views.students_groups_info, name='panel.students.groups.info'),
        path('kursanci/grupa/<str:uuid>/edytuj', panel_views.students_groups_edit, name='panel.students.groups.edit'),
        path('kursanci/grupa/<str:uuid>/usun', panel_views.students_groups_delete, name='panel.students.groups.delete'),

        path('kursanci/grupa/<str:uuid>/student/dodaj',
             panel_views.students_groups_students_add,
             name='panel.students.groups.students.add'),
    ]))
]
