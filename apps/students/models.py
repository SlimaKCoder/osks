from django.db import models
from main.models import BaseModel, User
from apps.payments.models import CoursePrice


class StudentGroup(BaseModel):
    start_date = models.DateField()

    def __str__(self):
        # Convert uuid to pseudo numeric id
        chars = self.id.strip()
        pseudo_id = 0

        for char in chars:
            pseudo_id += ord(char)

        return 'Grupa %s | %s' % (pseudo_id, self.start_date)

    class Meta:
        ordering = ['-start_date']


class Student(User):
    pkk = models.CharField(max_length=255, unique=True)
    address = models.CharField(max_length=255)
    group = models.ForeignKey(StudentGroup, models.SET_NULL, blank=True, null=True, related_name='students')
    individual_price = models.ForeignKey(CoursePrice, models.SET_NULL, blank=True, null=True)

    def __str__(self):
        return self.name + ' ' + self.surname + ' | ' + self.pesel
