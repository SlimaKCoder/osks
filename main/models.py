from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from uuid import uuid4, UUID
from base64 import urlsafe_b64decode, urlsafe_b64encode
from .managers import UserManager
from .fields import ShortUUIDField


class BaseModel(models.Model):
    _id = ShortUUIDField(db_column='id', primary_key=True, default=uuid4, editable=False)

    @property
    def id(self):
        return self._id

    @id.setter
    def id(self, value):
        self._id = value

    @id.getter
    def id(self):
        return urlsafe_b64encode(self._id.bytes).decode('utf8').rstrip('=\n')

    class Meta:
        abstract = True


class User(AbstractBaseUser, PermissionsMixin, BaseModel):
    name = models.CharField(max_length=100)
    surname = models.CharField(max_length=100)
    pesel = models.CharField(max_length=255, unique=True)
    phone = models.CharField(max_length=100)
    password = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)

    objects = UserManager()

    USERNAME_FIELD = 'pesel'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'
        ordering = ['surname', "name"]

    def get_full_name(self):
        full_name = '%s %s' % (self.name, self.surname)
        return full_name.strip()

    def get_short_name(self):
        return self.name
