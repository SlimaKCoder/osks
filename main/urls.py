"""osks URL Configuration"""

from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.views.decorators.cache import cache_page
from django.urls import path, include
from django.conf import settings
from . import views

panel_url_prefix = 'panel/'  # TODO: Move it to settings
portal_url_prefix = 'portal/'

urlpatterns = [
    path('', views.index_redirect, name='index'),
    path('', include('apps.portal.urls')),
    path('', include('apps.instructors.urls')),
    path('', include('apps.panel.urls')),
    path('', include('apps.students.urls')),
    path('', include('apps.drives.urls')),
    path('', include('apps.payments.urls')),
    path('', include('apps.lectures.urls')),
    path('zaloguj/', cache_page(60 * 15)(LoginView.as_view(template_name='auth/login.html')),  name='login'),
    path('wyloguj/', LogoutView.as_view(next_page='/'), name='logout'),
] + staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
