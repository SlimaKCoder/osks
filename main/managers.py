from django.contrib.auth.base_user import BaseUserManager


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, pesel, password, **extra_fields):
        if not pesel:
            raise ValueError('The given pesel must be set')
        user = self.model(pesel=pesel, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, pesel, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(pesel, password, **extra_fields)

    def create_superuser(self, pesel, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(pesel, password, **extra_fields)
