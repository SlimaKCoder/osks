from django.db import models
from uuid import UUID
from base64 import urlsafe_b64decode


class ShortUUIDField(models.UUIDField):
    def to_python(self, value):
        if not isinstance(value, UUID):
            try:
                value = UUID(bytes=urlsafe_b64decode(value + '=='))
            except (AttributeError, ValueError):
                pass
        return super().to_python(value)



