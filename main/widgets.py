from datetimewidget.widgets import \
    DateWidget as BaseDateWidget, \
    TimeWidget as BaseTimeWidget, \
    DateTimeWidget as BaseDateTimeWidget


class TimeWidget(BaseTimeWidget):
    _default_options = {'format': 'hh:ii'}
    _default_usel10n = True

    def __init__(self, attrs=None, options=_default_options, usel10n=_default_usel10n, bootstrap_version=None):
        super(TimeWidget, self).__init__(attrs, options, usel10n, bootstrap_version)


class DateWidget(BaseDateWidget):
    _default_options = {'format': 'yyyy-mm-dd'}
    _default_usel10n = True

    def __init__(self, attrs=None, options=_default_options, usel10n=_default_usel10n, bootstrap_version=None):
        super(DateWidget, self).__init__(attrs, options, usel10n, bootstrap_version)


class DateTimeWidget(BaseDateTimeWidget):
    _default_options = {'format': 'yyyy-mm-dd hh:ii'}
    _default_usel10n = True

    def __init__(self, attrs=None, options=_default_options, usel10n=_default_usel10n, bootstrap_version=None):
        super(DateTimeWidget, self).__init__(attrs, options, usel10n, bootstrap_version)
