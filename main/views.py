from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from apps.students.models import Student
from apps.instructors.models import Instructor


@login_required
def index_redirect(request):
    if isinstance(request.user, Instructor):
        return redirect('admin.index')
    if isinstance(request.user, Student):
        return redirect('student.index')

    return redirect('login')
