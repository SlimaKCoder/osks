from apps.students.models import Student
from apps.instructors.models import Instructor


class AuthBackend(object):
    @staticmethod
    def authenticate(username=None, password=None):
        try:
            instructor = Instructor.objects.get(pesel=username)
            if instructor.check_password(password):
                return instructor
        except Instructor.DoesNotExist:
            try:
                student = Student.objects.get(pesel=username)
                if student.check_password(password):
                    return student
            except Student.DoesNotExist:
                return None

    @staticmethod
    def get_user(user_id):
        try:
            return Instructor.objects.get(pk=user_id)
        except Instructor.DoesNotExist:
            try:
                return Student.objects.get(pk=user_id)
            except Student.DoesNotExist:
                return None




